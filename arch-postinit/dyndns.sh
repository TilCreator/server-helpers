#!/bin/sh
hostname="DOMAIN_PLACEHOLDER"
password="TOKEN_PLACEHOLDER"
ip=$(ip a show dev eth0 | grep inet6 | grep global  | grep -v deprecated | grep -v ' fd' | grep -v ' fe' | cut -d' ' -f6 | cut -d'/' -f1 | head -n 1)

touch /tmp/old_ip

if [ "$(cat /tmp/old_ip)" != "$ip" ]; then
    curl "https://dyn.dns.he.net/nic/update" -d "hostname=$hostname" -d "password=$password" -d "myip=$ip"
    echo -n $ip > /tmp/old_ip
fi

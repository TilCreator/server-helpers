#!/bin/sh

# Sets the fans of a PowerEdge R710 to full speed
# Usage:
# liftoff.sh

ipmitool raw 0x30 0x30 0x01 0x00  # Enable manual fan controll
ipmitool raw 0x30 0x30 0x02 0xff 0x64  # Set rpm to max, FULL POWER!
#ipmitool raw 0x30 0x30 0x01 0x01  # Disable manual fan controll

#!/bin/bash

# Updates a proxmox host and all containers with a update script at /root/update.sh
# Usage:
# update_add.sh [<container id>] [...]

#set -e

ids=$(pct list | grep running | cut -d' ' -f1)

if [ $# -ne 0 ]; then
    ids=$@
else
    apt full-upgrade --yes
    apt autoremove --yes
fi

for id in $ids; do
    printf "\n\n\nUpdating $id\n\n"
    echo "/root/update.sh" | lxc-attach $id
    if [ $? -ne 0 ]; then
        read -p "Error detected, plz press enter to continue" -r
    fi
done

read -p "Containers will be rebooted, press enter (or ctrl+c to cancel)" -r

for id in $ids; do
    echo Rebooting $id
    pct reboot $id
done

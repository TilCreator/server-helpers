#!/bin/env python3

# WIP
# Change custom display text on a PowerEdge R710
# Usage:
# display.py <text>

import os
import sys

text = sys.argv[1]
text_raw = [hex(ord(i)) for i in text]
cmd = 'ipmitool raw 0x6 0x58 193 0 0 %d %s'%(len(text), ' '.join(text_raw))
print(cmd)
os.system(cmd)
os.system('ipmitool raw 0x6 0x58 194 0 0')

#!/bin/bash

# Usage:
# arch-postinit.sh <container id>

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

#set -e

if [ -z "$1" ]; then
    echo Needs lxc id as \$1
    exit
fi


read -p "Container will stop, press enter" -r
pct stop $1


echo "Mounting container image"
mkdir /mnt/arch-postinit
umount /mnt/arch-postinit
mountpoint=$(rbd --pool pool-disks map vm-$1-disk-0)
mount $mountpoint /mnt/arch-postinit


echo "Backing up config"
cp /etc/pve/lxc/$1.conf /tmp


read -p "Unionize ssh (use same as host)? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    let unissh=true

    echo "mp0: /etc/ssh/authorized_keys,mp=/etc/ssh/authorized_keys" >> /etc/pve/lxc/$1.conf
else
    let unissh=false
fi


read -p "Add mountpoints? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo available:
    cat /etc/pve/lxc/*.conf | egrep "mp.: " | grep -v /etc/ssh/authorized_keys | cut -d' ' -f 2 | sort -u

    let mpid=1
    for mp in $(cat /etc/pve/lxc/*.conf | egrep "mp.: " | grep -v /etc/ssh/authorized_keys | cut -d' ' -f 2 | sort -u); do
        read -p "Add mountpoint \"$mp\"? " -n 1 -r
        echo
        if [[ $REPLY =~ ^[Yy]$ ]]; then
            echo "mp$mpid: $mp" >> /etc/pve/lxc/$1.conf
	    let mpid++
        fi
    done
fi


echo starting container
pct start $1
sleep 3


if [ $unissh ]; then
    cp $DIR/arch-postinit/sshd_config /mnt/arch-postinit/etc/ssh/sshd_config
fi


#read -p "Fix systemd-networkd >=244.1? " -n 1 -r
#echo
#if [[ $REPLY =~ ^[Yy]$ ]]; then
#    mkdir /mnt/arch-postinit/etc/systemd/system/systemd-networkd.service.d
#    cp $DIR/arch-postinit/systemd-networkd.service /mnt/arch-postinit/etc/systemd/system/systemd-networkd.service.d/lxc.conf
#    echo "ip a" | lxc-attach $1
#
#    read -p "Restart container (if no ip is available)? " -n 1 -r
#    echo
#    if [[ $REPLY =~ ^[Yy]$ ]]; then
#        lxc-stop $1
#        sleep 3
#        lxc-start $1
#        sleep 3
#    fi
#fi


read -p "Enable sshd? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "systemctl enable sshd.service; systemctl start sshd.service" | lxc-attach $1
    echo "ip a" | lxc-attach $1
fi


read -p "Add dyndns service+timer? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo -n "IP: "
    echo "ip a" | lxc-attach $1
    cp $DIR/arch-postinit/dyndns.* /mnt/arch-postinit/etc/systemd/system/
    read -p "Domain: " dyndns_domain
    read -p "Renew token: " dyndns_token
    sed -i.bak "s/DOMAIN_PLACEHOLDER/$dyndns_domain/g" /mnt/arch-postinit/etc/systemd/system/dyndns.sh
    sed -i.bak "s/TOKEN_PLACEHOLDER/$dyndns_token/g" /mnt/arch-postinit/etc/systemd/system/dyndns.sh
    echo "systemctl daemon-reload; systemctl enable --now dyndns.timer; systemctl start dyndns.service" | lxc-attach $1
fi


read -p "Init pacman-key add mirrorlist? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "pacman-key --init; pacman-key --populate archlinux" | lxc-attach $1
    cp $DIR/arch-postinit/mirrorlist /mnt/arch-postinit/etc/pacman.d/mirrorlist
    echo "pacman --noconfirm -Sy archlinux-keyring" | lxc-attach $1
fi


read -p "Install pacredir? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    cp $DIR/arch-postinit/pacman.conf /mnt/arch-postinit/etc/pacman.conf
    echo "pacman --noconfirm -R pacserve; pacman --noconfirm -Sy pacredir avahi pacman" | lxc-attach $1
    cp $DIR/arch-postinit/pacredir.conf /mnt/arch-postinit/etc/pacredir.conf
    echo "systemctl enable pacredir.service pacserve.service avahi-daemon.service; systemctl start pacredir.service pacserve.service avahi-daemon.service" | lxc-attach $1
    cp $DIR/arch-postinit/pacman.conf_pacredir /mnt/arch-postinit/etc/pacman.conf
    sleep 2
    echo "pacman --noconfirm -Sy" | lxc-attach $1
fi


read -p "Pacman update? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "pacman --noconfirm -Syu" | lxc-attach $1
fi


read -p "Install yay (with alias)? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "pacman --noconfirm -Sy sudo" | lxc-attach $1
    echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /mnt/arch-postinit/etc/sudoers
    echo 'alias yay="sudo -u yay yay"' >> /mnt/arch-postinit/root/.bashrc
    chmod +x /mnt/arch-postinit/root/.bashrc
    echo "pacman --noconfirm -Sy base-devel git go; useradd -m -d /home/yay -g wheel yay; cd /tmp; sudo -u yay git clone https://aur.archlinux.org/yay.git; cd yay; sudo -u yay makepkg; pacman --noconfirm -U yay-*.pkg.*; cd /; rm -rf /tmp/yay" | lxc-attach $1
fi


echo Umounting container
umount /mnt/arch-postinit
#rbd --pool pool-disks unmap $mountpoint  # will be used by the container by then


read -p "Restart container? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
    pct stop $1
    sleep 3
    pct start $1
fi

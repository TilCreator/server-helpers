#!/bin/bash

# Restarts networkd on all active containers
# Usage:
# restart_networkd.sh

#set -e

ids=$(pct list | grep running | cut -d' ' -f1)

if [ $# -ne 0 ]; then
    ids=$@
fi

for id in $ids; do
    printf "Restarting networkd of $id\n"
    echo "systemctl restart systemd-networkd" | lxc-attach $id
done
